-- ----------------------------
-- Sequence structure for id_message_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "id_message_seq";
CREATE SEQUENCE "id_message_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 20
 CACHE 1;
SELECT setval('"public"."id_message_seq"', 20, true);

-- ----------------------------
-- Sequence structure for id_user_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "id_user_seq";
CREATE SEQUENCE "id_user_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 247
 CACHE 1;
SELECT setval('"public"."id_user_seq"', 247, true);

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS "message";
CREATE TABLE "message" (
"id" int4 DEFAULT nextval('id_message_seq'::regclass) NOT NULL,
"transmitter_user" int4 NOT NULL,
"receiver_user" int4 NOT NULL,
"sent_date" timestamp(6) NOT NULL,
"seen_date" timestamp(6),
"message" text COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of message
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for userapp
-- ----------------------------
DROP TABLE IF EXISTS "userapp";
CREATE TABLE "userapp" (
"id" int4 DEFAULT nextval('id_user_seq'::regclass) NOT NULL,
"username" varchar(255) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of userapp
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Alter Sequences Owned By
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table message
-- ----------------------------
ALTER TABLE "message" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table userapp
-- ----------------------------
ALTER TABLE "userapp" ADD PRIMARY KEY ("id");
