package com.messagesservice.user;


import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by David-SW on 15/03/2017.
 */
@Entity
@Table(name="userapp")
public class User  implements Serializable {
    private long id;
    private String username;

    public User() {
    }

    public User(String username) {
        this.username = username;
    }

    @Id
    @SequenceGenerator(name="id_user_seq", sequenceName="id_user_seq", initialValue=1, allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="id_user_seq")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
