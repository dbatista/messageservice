package com.messagesservice.user;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by David-SW on 20/03/2017.
 */
public interface UserRepository extends CrudRepository<User, Long> {

    User findByUsername(String username);
}
