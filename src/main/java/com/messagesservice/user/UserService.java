package com.messagesservice.user;

import com.messagesservice.core.GlobalOperations;
import com.messagesservice.core.Response;
import com.messagesservice.core.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by David on 20/03/2017.
 */
@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public Response getUsers(){
        Response response = new Response();
        try {
            List<User> users = (List<User>) userRepository.findAll();
            if(users.size() == 0){
                response.setCode(StatusCode.NO_CONTENT);
                return response;
            }

            response.setCode(StatusCode.OK);
            response.setData(users);
        }catch (Exception e){
            response = GlobalOperations.getResponseErrors(e);
        }


        return response;
    }

    public Response createUser(String username) {
        Response response = new Response();
        try{
            if(userRepository.findByUsername(username.trim()) != null){
                response.setCode(StatusCode.NOT_FOUND);
                response.setMessage("User was not created because it already exists");
                return response;
            }

            User user = new User(username.trim());
            userRepository.save(user);
            response.setCode(StatusCode.CREATED);
            response.setMessage("User created correctly");


        }catch (Exception e){
            response = GlobalOperations.getResponseErrors(e);
        }

        return response;
    }

    public Response updateUser(String username, String newname) {
        Response response = new Response();
        try{
            User user = userRepository.findByUsername(username.trim());
            if(user == null){
                response.setCode(StatusCode.NOT_FOUND);
                response.setMessage("Username does not exist");
                return response;
            }

            user.setUsername(newname.trim());
            userRepository.save(user);
            response.setCode(StatusCode.OK);
            response.setMessage("User successfully modified");

        }catch (Exception e){
            response = GlobalOperations.getResponseErrors(e);
        }
        return response;
    }

    public Response deleteUser(String username) {
        Response response = new Response();
        try{
            User user = userRepository.findByUsername(username.trim());
            if(user == null){
                response.setCode(StatusCode.NOT_FOUND);
                response.setMessage("Username does not exist");
                return response;
            }

            userRepository.delete(user.getId());
            response.setCode(StatusCode.OK);
            response.setMessage("User successfully deleted");

        }catch (Exception e){
            response = GlobalOperations.getResponseErrors(e);
        }
        return response;
    }
}
