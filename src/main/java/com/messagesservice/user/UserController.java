package com.messagesservice.user;

import com.messagesservice.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by David-SW on 20/03/2017.
 */
@RestController
public class UserController {
    @Autowired
    private UserService userService;


    @RequestMapping(path="/api/user",  method = RequestMethod.GET)
    public Response getUsers() {
        return userService.getUsers();
    }

    @RequestMapping(path="/api/user/{username}", method = RequestMethod.POST)
    public Response createUser(@PathVariable String username) {
        return userService.createUser(username);
    }

    @RequestMapping(path="/api/user/{username}", method = RequestMethod.PUT)
    public Response updateUser(@PathVariable String username, @RequestParam("newname") String newname) {
        return userService.updateUser(username, newname);
    }

    @RequestMapping(path="/api/user/{username}", method = RequestMethod.DELETE)
    public Response deleteUser(@PathVariable String username) {
        return userService.deleteUser(username);
    }
}
