package com.messagesservice.core;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

/**
 * Created by David-SW on 20/03/2017.
 */
@JsonSerialize(using = ResponseSerializer.class)
public class Response<T> {
    private long code;
    private String status;
    private String message;
    private List<T> data;
    private T uniqueResult;

    public long getCode() {
        return code;
    }

    public void setCode(long code) {
        this.code = code;
        if(code >= 500 && code <= 599){
            this.status = "fail";
        }
        if(code >= 400 && code <= 499){
            this.status = "error";
        }
        status = "success";
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public T getUniqueResult() {
        return uniqueResult;
    }

    public void setUniqueResult(T uniqueResult) {
        this.uniqueResult = uniqueResult;
    }
}
