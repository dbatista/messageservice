package com.messagesservice.core;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

/**
 * Created by David-SW on 20/03/2017.
 */
public class ResponseSerializer extends JsonSerializer<Response> {

    public void serialize(Response response, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("code", response.getCode());
        jsonGenerator.writeStringField("status", response.getStatus());
        if(response.getMessage() != null) {
            jsonGenerator.writeStringField("message", response.getMessage());
        }
        if(response.getUniqueResult() != null){
            jsonGenerator.writeObjectField("data", response.getUniqueResult());
        }
        if(response.getData() != null){
            jsonGenerator.writeObjectField("data", response.getData());
        }

        jsonGenerator.writeEndObject();
    }
}
