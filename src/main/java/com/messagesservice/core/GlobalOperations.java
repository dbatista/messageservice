package com.messagesservice.core;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by David-SW on 20/03/2017.
 */
public class GlobalOperations {

    public static final DateFormat dateFormatDatabase = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    public static Response getResponseErrors(Exception e){
        Response response = new Response();
        response.setCode(StatusCode.INTERNAL_SERVER_ERROR);
        response.setMessage("If an error occurred our server, try later or contact an administrator.");

        List<String> errors = new ArrayList<String>();

        if(e.getMessage() != null){ errors.add(e.getMessage()); }

        if(e.getCause().getMessage() != null){ errors.add(e.getCause().getMessage()); }

        if(errors != null){ response.setData(errors); }

        return  response;
    }

}
