package com.messagesservice.message;

import com.messagesservice.core.GlobalOperations;
import com.messagesservice.core.Response;
import com.messagesservice.core.StatusCode;
import com.messagesservice.user.User;
import com.messagesservice.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletContext;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * Created by David on 20/03/2017.
 */
@Service
public class MessageService {
    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private UserRepository userRepository;

    public Response getByReceiver(String username){
        Response response = new Response();
        try {
            if(userRepository.findByUsername(username.trim()) == null){
                response.setCode(StatusCode.NOT_FOUND);
                response.setMessage("Receiver User not found");
                return response;
            }

            response.setCode(StatusCode.OK);
            List<Message> messages = messageRepository.findByReceiverUsername(username);
            if(messages == null){
                response.setCode(StatusCode.NO_CONTENT);
                response.setMessage("Messages not found for "+ username);
                return response;
            }

            for (Message message : messages) {
                if(message.getSeenDate() == null){
                    message.setSeenDate(new Date());
                    messageRepository.save(message);
                }
            }

            response.setData(messages);
        }catch (Exception e){
            response = GlobalOperations.getResponseErrors(e);
        }
        return response;
    }

    public Response sendMessage(String transmitter, String receiver, String message){
        Response response = new Response();
        try{
            User transmitterUser = userRepository.findByUsername(transmitter.trim());
            if(transmitterUser == null){
                response.setCode(StatusCode.NOT_FOUND);
                response.setMessage("Transmitter User not found");
                return response;
            }

            User receiverUser = userRepository.findByUsername(receiver.trim());
            if(receiverUser == null){
                response.setCode(StatusCode.NOT_FOUND);
                response.setMessage("Receiver User not found");
                return response;
            }

            Message newMessage = new Message();
            newMessage.setTransmitter(transmitterUser);
            newMessage.setReceiver(receiverUser);

            newMessage.setSentDate(new Date());
            newMessage.setMessage(message.trim());

            messageRepository.save(newMessage);

            response.setCode(StatusCode.OK);
            response.setMessage("The message was successfully sent to " + receiver.trim());
        }catch (Exception e){
            response = GlobalOperations.getResponseErrors(e);
        }
        return response;
    }
}
