package com.messagesservice.message;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by David-SW on 20/03/2017.
 */
public interface MessageRepository extends CrudRepository<Message, Long> {

    List<Message> findByReceiverUsername(String username);
}
