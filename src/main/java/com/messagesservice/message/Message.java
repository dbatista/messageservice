package com.messagesservice.message;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;
import com.messagesservice.user.User;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by David-SW on 20/03/2017.
 */
@Entity
@Table(name="message")
public class Message implements Serializable{
    private long id;
    private User transmitter;
    private User receiver;
    private Date sentDate;
    private Date seenDate;
    private String message;

    public Message() {
    }

    public Message(User transmitter, User receiver, Date sentDate, Date seenDate, String message) {
        this.transmitter = transmitter;
        this.receiver = receiver;
        this.sentDate = sentDate;
        this.seenDate = seenDate;
        this.message = message;
    }

    @Id
    @SequenceGenerator(name="id_message_seq", sequenceName="id_message_seq", initialValue=1, allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="id_message_seq")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "transmitter_user", nullable = false)
    public User getTransmitter() {
        return transmitter;
    }

    public void setTransmitter(User transmitter) {
        this.transmitter = transmitter;
    }

    @ManyToOne
    @JoinColumn(name = "receiver_user", nullable = false)
    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiverUser) {
        this.receiver = receiverUser;
    }

    @Basic
    @Column(name = "sent_date")
    @JsonSerialize(using = DateSerializer.class)
    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    @Basic
    @Column(name = "seen_date")
    @JsonSerialize(using = DateSerializer.class)
    public Date getSeenDate() {
        return seenDate;
    }

    public void setSeenDate(Date seenDate) {
        this.seenDate = seenDate;
    }

    @Basic
    @Column(name = "message")
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
