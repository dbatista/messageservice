package com.messagesservice.message;

import com.messagesservice.core.Response;
import com.messagesservice.message.Message;
import com.messagesservice.message.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by David-SW on 20/03/2017.
 */
@RestController
public class MessageController {

    @Autowired
    private MessageService messageService;

    @RequestMapping(path="/api/message/{username}",  method = RequestMethod.GET)
    public Response getMessagesByUser(@PathVariable String username) {
        return  messageService.getByReceiver(username);
    }

    @RequestMapping(path="/api/message/{transmitter}/{receiver}",  method = RequestMethod.POST)
    public Response sendMessage(@PathVariable String transmitter, @PathVariable String receiver, @RequestParam String message) {
        return  messageService.sendMessage(transmitter, receiver, message);
    }
}
