package com.messagesservice.message;

import com.messagesservice.user.User;
import com.messagesservice.user.UserRepository;
import com.messagesservice.user.UserService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by David on 20/03/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MessageControllerTest {
    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        messageRepository.deleteAll();
        userRepository.deleteAll();
        userRepository.save(new User("pedrotest"));
        userRepository.save(new User("mariatest"));

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @After
    public void afterSetup(){
        messageRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    public void getDavidsMessages() throws Exception{
        messageRepository.save(new Message(userRepository.findByUsername("mariatest"), userRepository.findByUsername("pedrotest"), new Date(), null, "Hola Pedro!" ));
        mockMvc.perform(get("/api/message/pedrotest"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.code").value(200))
                .andExpect(jsonPath("$.data", hasSize(1)));
    }

    @Test
    public void sendMessageMariaToPedro() throws Exception{
        mockMvc.perform(post("/api/message/mariatest/pedrotest?message=Hola pedro!"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.code").value(200));
    }

    @Test
    public void sendMessageReceiverNotExist() throws Exception{
        mockMvc.perform(post("/api/message/mariatest/juanito?message=Hola juanito :D!"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.code").value(404));
    }

    @Test
    public void sendMessageTransmitterNotExist() throws Exception{
        mockMvc.perform(post("/api/message/juanito/mariatest?message=Hola maria :D!"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.code").value(404));
    }


}
