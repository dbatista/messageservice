package com.messagesservice.user;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by David on 20/03/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserControllerTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        userRepository.deleteAll();
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @After
    public void afterSetup(){
        userRepository.deleteAll();
    }

    @Test
    public void createUserDavidtest() throws Exception{
        mockMvc.perform(post("/api/user/davidtest"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.code").value(201));
    }

    @Test
    public void createDuplicateUser() throws Exception{
        userRepository.save(new User("davidtest"));
        mockMvc.perform(post("/api/user/davidtest"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.code").value(404));
    }

    @Test
    public void getAllUsersTest() throws Exception{
        userRepository.save(new User("davidtest"));
        userRepository.save(new User("juantest"));
        mockMvc.perform(get("/api/user"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.code").value(200))
                .andExpect(jsonPath("$.data", hasSize(2)));;
    }

    @Test
    public void getNoUsers() throws Exception{
        mockMvc.perform(get("/api/user"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.code").value(204));
    }

    @Test
    public void updateUserDavidtestToJuantest() throws Exception{
        userRepository.save(new User("davidtest"));
        mockMvc.perform(put("/api/user/davidtest?newname=juantest"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.code").value(200));
    }

    @Test
    public void updateUserNotExist() throws Exception{
        mockMvc.perform(put("/api/user/davidtest?newname=juantest"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.code").value(404));
    }

    @Test
    public void deleteUserDavidtest() throws Exception{
        userRepository.save(new User("juantest"));
        mockMvc.perform(delete("/api/user/juantest"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.code").value(200));
    }

    @Test
    public void deleteUserNotExist() throws Exception{
        mockMvc.perform(delete("/api/user/davidtest"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.code").value(404));
    }
}
