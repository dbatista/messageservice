# **MESSAGES SERVICE**

## **Introduction**
Solution of the exercise [Messages list service](https://gist.github.com/timoteoponce/35ea20c6ed4a053f3c438b45f7be5f27)

## **What you’ll need**

* About 15 minutes
* A favorite text editor or IDE
* JDK 1.8 or later
* Maven 3.0+
* PostgreSQL 9.2+
* You can also import the code straight into your IDE: [IntelliJ IDEA](https://spring.io/guides/gs/intellij-idea/)

## **Build executable JAR**

* Create a database called **messageservice** and copy **BD_MessageService.sql**. To change a property of the connection go to:
```
messageservice\src\main\resources\application.properties
```
* With maven you can build the JAR file:
```
mvn clean install
```

* Then run the created JAR file:
```
java -jar target/messageservice-0.0.1-SNAPSHOT.jar
```

Logging output is displayed. The service should be up and running within a few seconds.

## Test the service

### User end-point

Get All Users
```
 [GET] http://localhost:8080/api/user/
```
Create a User
```
 [POST] http://localhost:8080/api/user/{username}
```
Rename a User
```
 [PUT] http://localhost:8080/api/user/{username}?newname={newname}
```
Delete a User
```
 [DELETE] http://localhost:8080/api/user/{username}
```

### Message end-point
Gets all User messages
```
 [GET] http://localhost:8080/api/message/{username}
```
Send a message to another User
```
 [POST] http://localhost:8080/api/message/{transmitter}/{receiver}?message={message}
```

###Example response
```Json
 {
     "code": 201,
     "status": "success",
     "message": "User created correctly",
     "data":[]
 }
```
**Explanation**

* code: Response error code
* status: Status depending on error code
* message: Message depending on operation or error
* data: Response data, can be unique result or array result